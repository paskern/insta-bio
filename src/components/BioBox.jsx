import React from 'react';
import {createUseStyles} from "react-jss";

const styles = createUseStyles({
  BioBox: {
    display: 'flex',
    boxShadow: '0 0 12px -6px',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '10rem',
    backgroundImage: props => props.img ? 'url('+props.img+')' : 'rgba(220, 220, 220, .5)',
    backgroundRepeat: 'no-repeat',
    backgroundPositionX: '50%',
    backgroundPositionY: '50%',
    backgroundSize: '100%',
    padding: '1rem',
    '&:hover': {
      backgroundSize: '110%'
    }
  }
})

function BioBox(props) {
  const classes = styles(props)
  return (
      <div className={classes.BioBox}>
        {props.children}
      </div>
  );
}

export default BioBox;