import './styles/App.scss';
import BioBox from "./components/BioBox";
import img from './img/me.png'
import frontendWeekly from './img/frontend-weekly.png'
import lego from './img/lego.png'
import fundMeKinder from './img/fundMeKinder.png'
import {createUseStyles} from "react-jss";

const styles = createUseStyles({
  misc: {
    width: '100%',
    alignItems: 'center',
    marginLeft: '2rem',
    display: 'flex'
  },

})

function App() {
  const classes = styles()
  return (
    <div className="App">
      <header>
        <a href='https://www.instagram.com/hh.pascal/' >
          <img src={img} alt='Bild von Pascal'/>
        </a>
        <div className={classes.misc}>
          <span>
            Pascals Bio
          </span>
        </div>
      </header>
      <main>
        <a data-tip='Schau dir an was Thorsten Klahold passiert ist und warum Lego was gegen kleine Händler hat.' href='https://youtu.be/oj10ikQEFlw'>
          <BioBox img={lego} />
        </a>
        <a data-tip='Spende für alternative Klemmbausteine die ohne gewinn und kostenlos an Kinder gehen!' href='https://gofund.me/bde12674'>
          <BioBox img={fundMeKinder} />
        </a>
        <a data-tip='Unterstütze Frontend Weekly auf Patreon.com' href='https://patreon.com/frontendweekly?utm_medium=social&utm_source=twitter&utm_campaign=creatorshare'>
          <BioBox img={frontendWeekly} />
        </a>
      </main>
      <footer>
        <a href='https://datenschutz.pascalkern.de'>Datenschutz</a>
      </footer>

    </div>
  );
}

export default App;
